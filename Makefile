#
# [Как собрать исходники]
# Make в Windows изначально не установлен.
# Что бы установить make на windows компьютер введите в PowerShell команды которые указаны ниже.
# Set-ExecutionPolicy Bypass -Scope Process -Force; [System.Net.ServicePointManager]::SecurityProtocol = [System.Net.ServicePointManager]::SecurityProtocol -bor 3072; iex ((New-Object System.Net.WebClient).DownloadString('https://community.chocolatey.org/install.ps1'))
# choco install make
# Congratulations, теперь вы установили make на windows машину.
#
# Required tools(Необходимые иструменты):
#	- git
#	- make
#	- python 3.7+
#
#	[Windows]
#	- Git Bash
#	- make
#
#	[Linux]
#	- python3-pip
#	- python3-venv
#	- libsdl2-dev
#
# Advance preparation(Предворительная подготовка):
#	git clone --depth=1 https://github.com/kitao/pyxel
#	cd pyxel
#

ROOT_DIR = .
MAIN_DIR = $(ROOT_DIR)/src
VENV_DIR = $(ROOT_DIR)/venv

BASEDIR=$(dirname "$0")

SHELL = /bin/bash

ifeq ($(COMSPEC),) #Linux or Unix
PYTHON = python3
PIP = pip3
VENV_SCRIPT = source $(VENV_DIR)/bin/activate
else # Windows
PYTHON = python
PIP = pip
VENV_SCRIPT = $(VENV_DIR)/Scripts/activate.bat
endif

build:
	$(PYTHON) -m  venv $(VENV_DIR)
	$(VENV_SCRIPT)
	$(PIP) install -r requirements.txt
	cd $(MAIN_DIR) && $(PYTHON) main.py

#sourse https://github.com/kitao/pyxel/blob/main/Makefile
