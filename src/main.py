from multiprocessing import Process, Queue
import os
import pygame.time
from pygame import *
import random
import time as t
import serial

START_BUTTON_PATH = "../images/start_btn.png"
BACKGROUND_PATH = "../images/back.jpg"
BIRD_PATH = "../images/bird.png"
PILLAR_UP_PATH = "../images/Stolb1.png"
PILLAR_DOWN_PATH = "../images/Stolb2.png"
SOUND_JUMP_PATH = "../sound/jump.mp3"


def game(queue, queue2):
    pygame.init()

    NULL = 0
    info_Object = pygame.display.Info()
    HEIGHT = info_Object.current_h
    WIDTH = info_Object.current_w
    window = display.set_mode((WIDTH, HEIGHT), RESIZABLE)
    window.fill("BlACK")
    display.set_caption("Flappy Bird")
    icon = image.load(BIRD_PATH)
    display.set_icon(icon)

    class GameSprite(sprite.Sprite):
        def __init__(self, picture, w, h, x, y, speed): #####
            super().__init__()                          ######
            self.height, self.weight = h, w
            self.start_x, self.start_y = x, y
            self.picture = picture
            self.image = transform.scale(image.load(self.picture), (self.weight, self.height))
            self.rect = self.image.get_rect()
            self.rect.x, self.rect.y = x, y
            self.speed = self.moving = speed

        def reset(self):
            window.blit(self.image, (self.rect.x, self.rect.y))

        def stop(self):
            self.speed = self.stopped

    class Functional_Sprite(sprite.Sprite):
        def __init__(self, picture, w, h, x, y):
            self.height, self.width = h, w
            self.image = transform.scale(image.load(picture), (w, h))
            self.rect = self.image.get_rect()
            self.x = self.rect.x = x
            self.y = self.rect.y = y

        def reset(self):
            window.blit(self.image, (self.rect.x, self.rect.y))

    class Birdy(GameSprite):
        def __init__(self, picture, w, h, x, y, speed, accel):
            super().__init__(picture, w, h, x, y, speed)
            self.accel = self.moving_accel = accel

        def stop(self):
            self.speed = self.accel = NULL

        def update(self):
            self.speed = self.moving
            self.accel = self.moving_accel

        def move(self):
            self.speed += self.accel
            self.rect.y += self.speed

        def finish_move(self):
            self.rect.y = self.start_y

    class BackSprites(GameSprite):
        def __init__(self, picture, w, h, x, y, speed):
            super().__init__(picture, w, h, x, y, speed)
            self.plus = 0

        def update(self):
            self.speed = self.moving + self.plus

        def stop(self):
            self.speed = NULL

        def move(self):
            self.rect.x -= self.speed

        def finish_move(self):
            self.rect.x = self.start_x
            self.rect.y = self.start_y

    wall1 = BackSprites(PILLAR_DOWN_PATH, 150, 250, WIDTH, NULL, 5)
    wall2 = BackSprites(PILLAR_UP_PATH, 200, 200, WIDTH, HEIGHT - 200, 6)
    walls = sprite.Group()
    walls.add(wall1)
    walls.add(wall2)

    bird = Birdy(BIRD_PATH, 50, 50, 100, 300, -10, 0.2)
    background1 = BackSprites(BACKGROUND_PATH, WIDTH, HEIGHT, 0, 0, 1)
    background2 = BackSprites(BACKGROUND_PATH, 600, 800, 600, 0, 1)
    start_button = Functional_Sprite(START_BUTTON_PATH, 400, 200, 600, 200)

    start_index = score = score_ind = 0

    GAME_FONT = font.SysFont('Century Gothic', 30)
    text_surface = GAME_FONT.render(str(score), False, (255, 255, 255))
    first_time = second_time = delta_time = 0
    run = True

    while run:
        if start_index < 1:
            bird.stop()
            wall1.stop()
            wall2.stop()
            background1.stop()
            background2.stop()
        pygame.time.delay(8)
        background1.reset()
        start_button.reset()
        walls.draw(window)
        bird.reset()
        bird.move()
        text_surface = GAME_FONT.render(str(score), False, (255, 255, 255))
        window.blit(text_surface, (20, 10))

        if delta_time >= 5000:
            wall1.plus += 0.5
            wall2.plus += 0.5
            first_time = time.get_ticks()

        if wall1.rect.right > NULL:
            wall1.move()
        else:
            wall1.finish_move()
            score_ind = 0

        if wall1.rect.x < WIDTH / 2 or score >= 1:
            wall2.move()
        if wall2.rect.right <= 0:
            wall2.finish_move()
            score_ind = 0

        if (wall1.rect.right <= bird.rect.x or wall2.rect.right <= bird.rect.x) and score_ind == 0:
            score += 1
            score_ind = 1

        hit = sprite.spritecollide(bird, walls, False)

        if hit or bird.rect.y <= NULL or bird.rect.y >= HEIGHT:
            start_index = 0
            bird.finish_move()
            wall1.finish_move()
            wall2.finish_move()
            start_button.rect.x = start_button.x
            start_button.rect.y = start_button.y

        for e in event.get():
            if e.type == QUIT:
                run = False
            elif e.type == KEYDOWN:
                if e.key == K_SPACE:
                    queue2.put(1)
                    if start_index == 0:
                        start_index += 1
                        score = 0
                        start_button.rect.x = start_button.x
                        start_button.rect.x = NULL - start_button.width
                        start_button.rect.y = NULL - start_button.height
                        first_time = time.get_ticks()
                        wall1.plus = wall2.plus = 0
                    wall1.update()
                    wall2.update()
                    bird.update()
                    background1.update()
                    background2.update()
        while queue.empty() == False:
            q = queue.get()
            if q == 1:
                if start_index == 0:
                    start_index += 1
                    score = 0
                    start_button.rect.x = start_button.x
                    start_button.rect.x = NULL - start_button.width
                    start_button.rect.y = NULL - start_button.height
                    first_time = time.get_ticks()
                    wall1.plus = wall2.plus = 0
                wall1.update()
                wall2.update()
                bird.update()
                background1.update()
                background2.update()

        second_time = time.get_ticks()
        delta_time = second_time - first_time
        display.update()


def read_serial(queue1, queue2):
    serial_connect = serial.Serial('COM5', 115200)
    old_value = new_value = 0
    while True:
        line = serial_connect.readline()
        if len(line) < 10:
            old_value = new_value
            new_value = int(line)
            print(int(line))
        if old_value == 0 and new_value == 1:
            queue1.put(1)
            queue2.put(1)

def beep():
    mixer.init()
    mixer.music.load(SOUND_JUMP_PATH)
    mixer.music.play()

def go_beep(queue):
    while True:
        t.sleep(0.05)
        while queue.empty() == False:
            print("hello from внутренний цикл бипалки")
            q = queue.get()
            if q == 1:
                beep()


def calibration():
    timestamp = t.time()
    timestampSurge = timestamp + 5
    timestampChill = timestampSurge + 3
    timestampEnd = timestampChill + 2

    while True:
        t.sleep(0.1)
        timestamp = t.time()

        if timestamp < timestampSurge:
            print('Расслабьте руку, положите ее на стол')
            continue

        if timestamp < timestampChill:
            print('Согни руку со всей силы')
            continue

        if timestamp < timestampEnd:
            print('Расслабьте руку, положите ее на стол')
            continue

        return

def ready_beep():
    beep()
    t.sleep(0.3)
    beep()
    t.sleep(0.2)
    beep()

if __name__ == '__main__':
    queue = Queue()
    queue2 = Queue()
    read_serial_process = Process(target=read_serial, args=(queue, queue2,))
    go_beep_process = Process(target=go_beep, args=(queue2, ))
    read_serial_process.start()
    go_beep_process.start()
    game_process = Process(target=game, args=(queue, queue2))

    calibration()
    ready_beep()

    game_process.start()



